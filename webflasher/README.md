# flow3r Web Flasher Tool

```
!!! Chrome only !!!
```

Flashes the flow3r badge from the browser. Available releases are loaded from a remote manifest. See configuration.

## Build

The flasher uses `npm` to handle build and dependencies and `esbuild` to bundle.

To build, run

```
npm run build
```

In most cases, running a clean first is a good idea:
```
npm run clean && npm run build
```

## Run locally

The flasher can run locally against firmware images placed in `static/api`. For this, it provides a dummy manifest in `static/api/releases.json`.

If you want to run against a local firmware build, you can copy the image files into the `static/api` directory:
```
FIRMWARE_REPO=/path/to/your/repo
cp $FIRMWARE_REPO/recovery/build/bootloader/bootloader.bin static/api
cp $FIRMWARE_REPO/recovery/build/partition_table/partition-table.bin static/api
cp $FIRMWARE_REPO/recovery/build/flow3r-recovery.bin static/api
cp $FIRMWARE_REPO/build/flow3r.bin static/api
```

You can then use the built-in HTTP server to serve a local version of the flasher:
```
npm run clean && npm run build && npm run serve
```

## Configuration

The URL to the `releases.json` manifest is specified in `src/main.ts` (`manifestUrl`).

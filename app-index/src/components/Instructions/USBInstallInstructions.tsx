import { Component } from "solid-js"
import { flash } from "../../helpers/flasher"
import { AppEntryType } from "../../backendSchema"

const USBInstallInstructions: Component<{
    app: AppEntryType
}> = (props) => {
    return (
        <div class="flex flex-col items-center gap-4 ">
            <a
                class="btn btn-secondary flex-1 text-center dark:text-black"
                onclick={() => flash(props.app.downloadUrl, alert)}
            >
                <i class="fa-brands fa-usb mr-2"></i>
                Install app
            </a>
            <div class="mx-8 space-y-4 text-lg">
                <ol class="list-decimal space-y-2">
                    <li>
                        <b>Connect your flow3r</b> using USB.
                    </li>
                    <li>
                        <b>Turn on</b> flow3r.
                    </li>
                    <li>
                        Press <b>install</b> and <b>wait</b> till install
                        finished and <b>flow3r restarts</b>.
                    </li>
                </ol>
            </div>
        </div>
    )
}

export default USBInstallInstructions

import { Component } from "solid-js"
import AppSeed from "./AppSeed"

const AppSeedInstructions: Component<{
    appName: string
    appCode: string
}> = (props) => {
    return (
        <div class="flex flex-col items-center gap-4 ">
            <AppSeed seed={props.appCode} />
            <div class="mx-8 space-y-4 text-lg">
                <ol class="list-decimal space-y-2">
                    <li>
                        Make sure you're running <b>firmware version 1.1.0</b>{" "}
                        or newer. Update{" "}
                        <a
                            class="underline"
                            href="https://git.flow3r.garden/flow3r/flow3r-firmware/-/releases"
                        >
                            here
                        </a>
                        .
                    </li>
                    <li>
                        Go to <b>System</b> &#10140; <b>Settings</b> and select{" "}
                        <b>Connect Camp WiFi</b> to switch on WiFi.
                    </li>
                    <li>
                        Proceed to the app store <b>System</b> &#10140;{" "}
                        <b>Get Apps</b>.
                    </li>
                    <li>
                        In the app store, choose <b>Enter flow3r seed</b>.
                    </li>
                    <li>Use the five top-petals to enter the code below.</li>
                </ol>
            </div>
        </div>
    )
}

export default AppSeedInstructions

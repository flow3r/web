import { Component } from "solid-js"

const ManualInstallInstructions: Component<{
    downloadUrl: string
}> = (props) => {
    return (
        <div class="flex flex-col items-center gap-4 ">
            <a
                class="btn btn-secondary flex-1 text-center dark:text-black"
                href={props.downloadUrl}
            >
                <i class="fa-regular fa-floppy-disk mr-2"></i>
                Download app
            </a>
            <div class="mx-8 space-y-4 text-lg">
                <ol class="list-decimal space-y-2">
                    <li>
                        <b>Download</b> zipped App files
                    </li>
                    <li>
                        Mount the Flash drive of your flow3r. You can do so by
                        entering: <b>System {">"} Disk Mode (Flash)</b>
                    </li>
                    <li>
                        <b>Unzip</b> them and place the folder containing the
                        App in the sys directory on the "Flash" filesystem.
                    </li>
                </ol>
            </div>
        </div>
    )
}

export default ManualInstallInstructions

import { Component, For, createSignal } from "solid-js"
import { playDTMF } from "../../helpers/audio"

const AppSound: Component<{ seed: String }> = (props) => {
    const [sectionPlaying, setSectionPlaying] = createSignal(-2)

    const playSeed = async (seed: String) => {
        setSectionPlaying(-1)
        await playDTMF("#")
        await playDTMF("#")
        let seedIndex = 0
        for (const seedChar of seed.split("")) {
            await playDTMF(seedChar)
            setSectionPlaying(seedIndex)
            seedIndex += 1
        }
        await playDTMF("#")
        await playDTMF("#")
        setSectionPlaying(-2)
    }

    const genRandom = () => {
        return [...Array(4)].map(() => Math.floor(Math.random() * 9))
    }

    const getSectionColor = (seedChar: string) => {
        const sectionColors = [
            "#216CFF",
            "#21FFF2",
            "#EDFB48",
            "#3FFF21",
            "#FB48C4",
        ]
        return sectionColors[parseInt(seedChar, 10)]
    }

    return (
        <div
            class="border-gray flex items-center gap-2 rounded-lg border-2 border-solid px-10 py-2"
            onclick={() => playSeed(props.seed)}
        >
            <button class="h-16 w-16 rounded-full bg-go-green dark:text-black">
                {sectionPlaying() >= -1 ? (
                    <i class="fa-solid fa-pause"></i>
                ) : (
                    <i class="fa-solid fa-play"></i>
                )}
            </button>
            <div class="flex flex-1 items-center justify-evenly">
                <For each={props.seed.split("")}>
                    {(seedChar, sectionIdx) => (
                        <For each={genRandom()}>
                            {(item, idx) => (
                                <div
                                    class="w-1 rounded-full bg-black dark:bg-white sm:m-1"
                                    style={`height: ${
                                        item * 3
                                    }px; background-color: ${
                                        sectionPlaying() == sectionIdx()
                                            ? getSectionColor(seedChar)
                                            : ""
                                    };`}
                                ></div>
                            )}
                        </For>
                    )}
                </For>
            </div>
        </div>
    )
}

export default AppSound

import { Component, For } from "solid-js"
import { ShameListType, ShameType } from "../shameListBackend"

export const ShameList: Component<{ data: ShameListType }> = (props) => {
    return (
        <div>
            <h2 class="text-center text-xl">
                <b>Official list of wilted repos</b>{" "}
                <i class="fa-solid fa-plant-wilt"></i>
            </h2>
            <p class="mb-2 text-center">
                generated at {props.data.isodatetime}
            </p>
            <table class="mb-4 table-auto border-separate border-spacing-x-4">
                <tbody class="table">
                    <tr>
                        <th class="text-right">Repo</th>
                        <th class="text-left">Problem</th>
                    </tr>
                    <For each={props.data.shame}>
                        {(shameEntry: ShameType) => (
                            <ShameListEntry data={shameEntry} />
                        )}
                    </For>
                </tbody>
            </table>
        </div>
    )
}

export const ShameListEntry: Component<{ data: ShameType }> = (props) => {
    return (
        <tr>
            <td class="mr-4 text-right">
                <a href={`https://git.flow3r.garden/${props.data.repo}`}>{props.data.repo}</a>
            </td>

            <td>
                <p>{props.data.errorMsg}</p>
            </td>
        </tr>
    )
}

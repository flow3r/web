import { parseAppListJSON } from "../backendSchema"
import { parseShameListJSON } from "../shameListBackend"

const apiUrl = "https://flow3r.garden/api/apps.json"
const shameUri = "https://flow3r-api.geile.software/api/apps/shame.json"

export const fetchAppList = async () => {
    return fetch(apiUrl)
        .then((res) => res.json())
        .then((data) => parseAppListJSON(data))
}

export const fetchShameList = async () => {
    return fetch(shameUri)
        .then((res) => res.json())
        .then((data) => parseShameListJSON(data))
}
